FROM alpine:latest
RUN apk update && apk add --no-cache clamav clamav-libunrar amavis p7zip perl-io-socket-ssl cabextract lha
RUN mkdir -p /data/conf

COPY conf /data/conf
COPY start.sh /start.sh

RUN chown -R root:root /data/conf && chmod -R 0755 /data/conf

EXPOSE 10024/tcp
VOLUME ["/data/database"]

CMD /start.sh
