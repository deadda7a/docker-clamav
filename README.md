# ClamAV and AMaViS in Docker

1. Set the enviroment variables
  1. DOMAIN to your domain (The hostname is amavis.DOMAIN)
  2. SMTPSERVER to your SMTP host
  3. SMTPPORT to the port of your SMTP server
2. The port is 10024
3. If you want to avoid to download fresh data every time you recreate the container, make /data/database persistent
