#!/bin/sh

if [ -z "${DOMAIN}" ]; then
  echo "No Domain set!"
  exit 1
fi

if [ -z "${SMTPSERVER}" ]; then
  echo "No SMTP server set!"
  exit 1
fi

if [ -z "${SMTPPORT}" ]; then
  echo "No SMTP port set!"
  exit 1
fi

echo "Set config!"
echo "Domain: ${DOMAIN}"
echo "SMTP Server: ${SMTPSERVER}"
echo "SMTP Port: ${SMTPPORT}"
sed -i "s/((DOMAIN))/${DOMAIN}/g" "/data/conf/amavisd.conf"
sed -i "s/((SMTPSERVER))/${SMTPSERVER}/g" "/data/conf/amavisd.conf"
sed -i "s/((SMTPPORT))/${SMTPPORT}/g" "/data/conf/amavisd.conf"

if [ ! -f "/data/main.cvd" ]; then
  echo "Starting primary virus DB download!"
  freshclam --config-file=/data/conf/freshclam.conf
fi

echo "Starting the update daemon!"
freshclam --config-file=/data/conf/freshclam.conf -d -c 6

echo "Starting clamav!"
clamd --config-file=/data/conf/clamd.conf --foreground=false

echo "Starting AMaViS"
amavisd -i amavis.${DOMAIN} -c /data/conf/amavisd.conf debug
