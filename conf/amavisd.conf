use strict;

@bypass_spam_checks_maps = (1);

$max_servers  = 2;
$daemon_user  = 'amavis';
$daemon_group = 'amavis';

$mydomain = '((DOMAIN))';

$MYHOME             = '/var/amavis';
$TEMPBASE           = "$MYHOME/tmp";
$ENV{TMPDIR}        = $TEMPBASE;
$QUARANTINEDIR      = '/var/amavis/quarantine';
$lock_file          = "$MYHOME/var/amavisd.lock";
$pid_file           = "$MYHOME/var/amavisd.pid";
$log_level          = 0;
$log_recip_templ    = undef;
$do_syslog          = 1;
$syslog_facility    = 'mail';
$enable_db          = 0;
@local_domains_maps = ( [".$mydomain"] );
@mynetworks = qw( 127.0.0.0/8 [::1] [FE80::]/10 [FEC0::]/10 10.0.0.0/8 172.16.0.0/12 192.168.0.0/16 172.16.0.0/12);
$unix_socketname       = "$MYHOME/amavisd.sock";
$inet_socket_port      = 10024;
$inet_socket_bind      = "0.0.0.0";
$enable_dkim_verification = 1;

$policy_bank{'MYNETS'} = {
    originating           => 1,
    os_fingerprint_method => undef,
};

@inet_acl = @mynetworks;

$interface_policy{'10026'} = 'ORIGINATING';

$policy_bank{'ORIGINATING'} = {
    originating                     => 1,
    allow_disclaimers               => 1,
    warnbadhsender                  => 0,
    smtpd_discard_ehlo_keywords     => ['8BITMIME'],
    bypass_banned_checks_maps       => [1],
    terminate_dsn_on_notify_success => 0,
};

$interface_policy{'SOCK'}   = 'AM.PDP-SOCK';
$policy_bank{'AM.PDP-SOCK'} = {
    protocol              => 'AM.PDP',
    auth_required_release => 0,
};

$sa_tag_level_deflt               = 2.0;
$sa_tag2_level_deflt              = 6.2;
$sa_kill_level_deflt              = 6.9;
$sa_dsn_cutoff_level              = 10;
$sa_crediblefrom_dsn_cutoff_level = 18;
$penpals_bonus_score              = 8;
$penpals_threshold_high           = $sa_kill_level_deflt;
$bounce_killer_score              = 100;

$sa_mail_body_size_limit = 400 * 1024;
$sa_local_tests_only     = 0;

$virus_admin = "virusalert\@$mydomain";

$mailfrom_notify_admin     = "virusalert\@$mydomain";
$mailfrom_notify_recip     = "virusalert\@$mydomain";
$mailfrom_notify_spamadmin = "spam.police\@$mydomain";
$mailfrom_to_quarantine    = '';

@addr_extension_virus_maps      = ('virus');
@addr_extension_banned_maps     = ('banned');
@addr_extension_spam_maps       = ('spam');
@addr_extension_bad_header_maps = ('badh');

$path = '/usr/local/sbin:/usr/local/bin:/usr/sbin:/sbin:/usr/bin:/bin';

$MAXLEVELS           = 14;
$MAXFILES            = 3000;
$MIN_EXPANSION_QUOTA = 100 * 1024;
$MAX_EXPANSION_QUOTA = 500 * 1024 * 1024;

$sa_spam_subject_tag              = '***Spam*** ';
$defang_virus                     = 1;
$defang_banned                    = 1;
$defang_by_ccat{ CC_BADH . ",3" } = 1;
$defang_by_ccat{ CC_BADH . ",5" } = 1;
$defang_by_ccat{ CC_BADH . ",6" } = 1;

$myhostname = 'amavis.((DOMAIN))';

$notify_method  = 'smtp:[((SMTPSERVER))]:((SMTPPORT))';
$forward_method = 'smtp:[((SMTPSERVER))]:((SMTPPORT))';

$final_virus_destiny      = D_DISCARD;
$final_banned_destiny     = D_DISCARD;
$final_spam_destiny       = D_REJECT;
$final_bad_header_destiny = D_PASS;

@keep_decoded_original_maps = (
    new_RE(
        qr'^MAIL$', qr'^MAIL-UNDECIPHERABLE$',
        qr'^(ASCII(?! cpio)|text|uuencoded|xxencoded|binhex)'i,
    )
);

$banned_filename_re = new_RE(
    qr'^\.(exe-ms|dll)$',
    [ qr'^\.(rpm|cpio|tar)$' => 0 ],

    qr'.\.(pif|scr)$'i,

    qr'^application/x-msdownload$'i,
    qr'^application/x-msdos-program$'i,
    qr'^application/hta$'i,

qr'^(?!cid:).*\.[^./]*[A-Za-z][^./]*\.\s*(exe|vbs|pif|scr|bat|cmd|com|cpl|dll)[.\s]*$'i,

    qr'.\.(exe|vbs|pif|scr|cpl)$'i,
);

@score_sender_maps = (
    {
        '.' => [

            new_RE(
                [
                    qr'^(bulkmail|offers|cheapbenefits|earnmoney|foryou)@'i =>
                      5.0
                ],
                [
qr'^(greatcasino|investments|lose_weight_today|market\.alert)@'i
                      => 5.0
                ],
                [
qr'^(money2you|MyGreenCard|new\.tld\.registry|opt-out|opt-in)@'i
                      => 5.0
                ],
                [
qr'^(optin|saveonlsmoking2002k|specialoffer|specialoffers)@'i
                      => 5.0
                ],
                [
qr'^(stockalert|stopsnoring|wantsome|workathome|yesitsfree)@'i
                      => 5.0
                ],
                [ qr'^(your_friend|greatoffers)@'i             => 5.0 ],
                [ qr'^(inkjetplanet|marketopt|MakeMoney)\d*@'i => 5.0 ],
            ),

            {
                'nobody@cert.org'                               => -3.0,
                'cert-advisory@us-cert.gov'                     => -3.0,
                'owner-alert@iss.net'                           => -3.0,
                'slashdot@slashdot.org'                         => -3.0,
                'securityfocus.com'                             => -3.0,
                'ntbugtraq@listserv.ntbugtraq.com'              => -3.0,
                'security-alerts@linuxsecurity.com'             => -3.0,
                'mailman-announce-admin@python.org'             => -3.0,
                'amavis-user-admin@lists.sourceforge.net'       => -3.0,
                'amavis-user-bounces@lists.sourceforge.net'     => -3.0,
                'spamassassin.apache.org'                       => -3.0,
                'notification-return@lists.sophos.com'          => -3.0,
                'owner-postfix-users@postfix.org'               => -3.0,
                'owner-postfix-announce@postfix.org'            => -3.0,
                'owner-sendmail-announce@lists.sendmail.org'    => -3.0,
                'sendmail-announce-request@lists.sendmail.org'  => -3.0,
                'donotreply@sendmail.org'                       => -3.0,
                'ca+envelope@sendmail.org'                      => -3.0,
                'noreply@freshmeat.net'                         => -3.0,
                'owner-technews@postel.acm.org'                 => -3.0,
                'ietf-123-owner@loki.ietf.org'                  => -3.0,
                'cvs-commits-list-admin@gnome.org'              => -3.0,
                'rt-users-admin@lists.fsck.com'                 => -3.0,
                'clp-request@comp.nus.edu.sg'                   => -3.0,
                'surveys-errors@lists.nua.ie'                   => -3.0,
                'emailnews@genomeweb.com'                       => -5.0,
                'yahoo-dev-null@yahoo-inc.com'                  => -3.0,
                'returns.groups.yahoo.com'                      => -3.0,
                'clusternews@linuxnetworx.com'                  => -3.0,
                lc('lvs-users-admin@LinuxVirtualServer.org')    => -3.0,
                lc('owner-textbreakingnews@CNNIMAIL12.CNN.COM') => -5.0,
            },
        ],
    }
);

@decoders = (
    [ 'mail', \&do_mime_decode ],
    [ 'F',  \&do_uncompress, [ 'unfreeze',   'freeze -d', 'melt', 'fcat' ] ],
    [ 'Z',  \&do_uncompress, [ 'uncompress', 'gzip -d',   'zcat' ] ],
    [ 'gz', \&do_uncompress, 'gzip -d' ],
    [ 'gz', \&do_gunzip ],
    [ 'bz2', \&do_uncompress, 'bzip2 -d' ],
    [ 'xz', \&do_uncompress, [ 'xzdec', 'xz -dc', 'unxz -c', 'xzcat' ] ],
    [
        'lzma',
        \&do_uncompress,
        [
            'lzmadec',  'xz -dc --format=lzma',
            'lzma -dc', 'unlzma -c',
            'lzcat',    'lzmadec'
        ]
    ],
    [ 'lrz', \&do_uncompress, [ 'lrzip -q -k -d -o -', 'lrzcat -q -k' ] ],
    [ 'lzo', \&do_uncompress, 'lzop -d' ],
    [ 'lz4', \&do_uncompress, ['lz4c -d'] ],
    [ 'rpm', \&do_uncompress, [ 'rpm2cpio.pl', 'rpm2cpio' ] ],
    [ [ 'cpio', 'tar' ], \&do_pax_cpio, [ 'pax', 'gcpio', 'cpio' ] ],
    [ 'deb', \&do_ar, 'ar' ],
    [ 'rar',  \&do_unrar,      [ 'unrar',   'rar' ] ],
    [ 'arj',  \&do_unarj,      [ 'unarj',   'arj' ] ],
    [ 'arc',  \&do_arc,        [ 'nomarch', 'arc' ] ],
    [ 'zoo',  \&do_zoo,        [ 'zoo',     'unzoo' ] ],
    [ 'doc',  \&do_ole,        'ripole' ],
    [ 'cab',  \&do_cabextract, 'cabextract' ],
    [ 'tnef', \&do_tnef_ext,   'tnef' ],
    [ 'tnef', \&do_tnef ],
    [ [ 'zip', 'kmz' ], \&do_7zip, [ '7za', '7z' ] ],
    [ [ 'zip', 'kmz' ], \&do_unzip ],
    [ '7z',               \&do_7zip, [ '7zr', '7za', '7z' ] ],
    [ [qw(gz bz2 Z tar)], \&do_7zip, [ '7za', '7z' ] ],
    [ [qw(xz lzma jar cpio arj rar swf lha iso cab deb rpm)], \&do_7zip, '7z' ],
    [ 'exe', \&do_executable, [ 'unrar', 'rar' ], 'lha', [ 'unarj', 'arj' ] ],
);

@av_scanners = (
    [
        'ClamAV-clamd',
        \&ask_daemon,
        [ "CONTSCAN {}\n", "/tmp/clamd.sock" ],
        qr/\bOK$/m,
        qr/\bFOUND$/m,
        qr/^.*?: (?!Infected Archive)(.*) FOUND$/m
    ],
);

@av_scanners_backup = (
    [
        'ClamAV-clamscan',
        'clamscan',
        "--stdout --no-summary -r --tempdir=$TEMPBASE {}",
        [0],
        qr/:.*\sFOUND$/m,
        qr/^.*?: (?!Infected Archive)(.*) FOUND$/m
    ],
);

1;
